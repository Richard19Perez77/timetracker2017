package radical.time.tracker;

/**
 * Created by Richard on 5/21/2017.
 */
class TimeAmounts {

    public int days;
    public int hours;
    public int minutes = 0;
    public int seconds = 0;

    public TimeAmounts(int d, int h, int m, int s) {
        seconds = s;
        minutes = m;
        hours = h;
        days = d;

        if (seconds >= 60 || seconds <= -60) {
            minutes += seconds / 60;
            seconds = seconds % 60;
        }

        if (minutes >= 60 || minutes <= -60) {
            hours += minutes / 60;
            minutes = minutes % 60;
        }

        if (hours >= 24 || hours <= -24) {
            days += hours / 24;
            hours = hours % 24;
        }
    }
}
