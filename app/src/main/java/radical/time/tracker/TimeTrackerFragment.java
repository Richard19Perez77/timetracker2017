package radical.time.tracker;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.TransitionDrawable;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Time Tracker Fragment holding the logic for our application.
 */
public class TimeTrackerFragment extends Fragment {

        /**
         * Use for setting a transparent background to the web view.
         */
        private final static int TRANSPARENT = 0;

        /**
         * Use for one second breaks in keeping the timer thread.
         */
        private final static int ONE_SECOND = 1000;

        /**
         * The user's records are printed out in detail in this rootView.
         */
        private WebView webview;

        /**
         * The slot buttons are different instances of time tracking.
         */
        private Button slot1Button;
        private Button slot2Button;

        /**
         * Allow for the user to give input when performing actions such as delete
         * that may have consequences.
         */
        private AlertDialog.Builder alert;

        /**
         * Use for testing.
         */
        public AlertDialog testAlert;

        /**
         * Creates a small window alert for notifications.
         */
        private Toast myToast;

        /**
         * Displays the current timer increment
         */
        public TextView timerTextView;

        /**
         * Static variables for database transactions
         */
        private static final String DATABASE_NAME = "timetrackerdatabase.db";
        private static final String TIME_TRACKER_TABLE = "time_records";
        private static final String COLUMN_ID = "ID";
        private static final String COLUMN_DAYS = "DAYS";
        private static final String COLUMN_HOURS = "HOURS";
        private static final String COLUMN_MINUTES = "MINUTES";
        private static final String COLUMN_SECONDS = "SECONDS";
        private static final String COLUMN_COMMENT = "COMMENT";
        private static final String COLUMN_NAME = "NAME";
        private static final String COLUMN_SLOT = "SLOT";

        private static final String DEFAULT_SLOT_ONE = "Tracking 1";
        private static final String DEFAULT_SLOT_TWO = "Tracking 2";

        /**
         * Used to pull field values and perform time calculations.
         */
        private int days;
        private int hours;
        private int minutes;
        private int seconds;

        private long currentTimeSpan = 0;

        private int currentSlot;

        private int previousSlot;

        private int records;

        private String slot1Name;
        private String slot2Name;
        private String currentSlotName;

        private SQLiteDatabase scoreDB;

        private EditText renameInput;

        private TimerThread timerThread;

        private Long timeToAdd = 0L;

        /**
         * The transition is the animation when pressing a button or on release of a
         * button press.
         */
        private TransitionDrawable trans1;
        private TransitionDrawable trans2;
        private TransitionDrawable transa;
        private TransitionDrawable transb;
        private TransitionDrawable timerTrans;

        private static final int TRANSITION_DURATION = 500;

        /**
         * Used to tell if the timer should be incrementing seconds.
         */
        boolean isTimerRunning;

        /**
         * The time is stored as the initial startDate and pause, time will increase even
         * if the application is closed.
         */
        private Date startDate;
        private Date updateDate;

        private SharedPreferences sharedPreferences;

        private int timerSecond;
        private int timerMinute;
        private int timerHour;
        private int timerDay;

        private TextView sTextView;
        private TextView mTextView;
        private TextView hTextView;
        private TextView dTextView;

        /**
         * On long press of the timer button we can send the time to the input
         * fields but we need to format and display it using this string.
         */
        private String timerTextString;

        /**
         * Shared preferences variables.
         */
        private static final String MY_PREFERENCES = "MyPrefs";
        private static final String TIMER_RUNNING = "timerrunning";
        private static final String CURRENT_TIME_SPAN = "currentTimeSpan";
        private static final String EDITTEXT_DAYS = "days";
        private static final String EDITTEXT_HOURS = "hours";
        private static final String EDITTEXT_MINUTES = "minutes";
        private static final String EDITTEXT_SECONDS = "seconds";
        private static final String CLOSING_TIME = "closingTime";

        /**
         * The rootView object will be needed to show toast messages in the Fragment.
         */
        private View rootView;

        private Date pauseDate;

    /**
     * On create of the {@link radical.time.tracker.TimeTrackerFragment} we can set values that the
     * user had in the text fields.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

        // Check whether we're recreating a previously destroyed instance
        if (savedInstanceState != null) {

            // Restore value of members from saved state
            setSecondsText(savedInstanceState
                    .getString(getString(R.string.seconds)));
            setMinutesText(savedInstanceState
                    .getString(getString(R.string.minutes)));
            setHoursText(savedInstanceState.getString(getString(R.string.hours)));
            setDaysText(savedInstanceState.getString(getString(R.string.days)));
        }

        setHasOptionsMenu(true);
    }

    /**
     * We can create the view by inflating the layout and then using the
     * time tracker class to set up the values displayed to the user.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_my, container,
                false);

        //reference views that hold time increments
        sTextView = (TextView) rootView.findViewById(R.id.secondsInputText);
        mTextView = (TextView) rootView.findViewById(R.id.minutesInputText);
        hTextView = (TextView) rootView.findViewById(R.id.hoursInputText);
        dTextView = (TextView) rootView.findViewById(R.id.daysInputText);

        currentSlot = 1;
        previousSlot = 1;

        slot1Name = DEFAULT_SLOT_ONE;
        slot2Name = DEFAULT_SLOT_TWO;

        webview = (WebView) rootView.findViewById(R.id.webView1);
        webview.setBackgroundColor(TRANSPARENT);

        timerTextView = (TextView) rootView.findViewById(R.id.timerText);

        Button timerButton = (Button) rootView.findViewById(R.id.timerButton);
        timerButton.setBackgroundResource(R.drawable.transitionbottombuttons);
        timerTrans = (TransitionDrawable) timerButton.getBackground();
        timerTrans.startTransition(TRANSITION_DURATION);
        timerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timerToggle();
                timerTrans.startTransition(TRANSITION_DURATION);
            }
        });

        timerButton.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                createMoveTimeDialog();
                return false;
            }
        });

        // reference the delete button
        Button delAllButton = (Button) rootView.findViewById(R.id.delAllButton);
        delAllButton.setBackgroundResource(R.drawable.transitionbottombuttons);

        // reference the transition drawable to be manipulated
        TransitionDrawable transitionDrawable = (TransitionDrawable) delAllButton.getBackground();
        transitionDrawable.startTransition(TRANSITION_DURATION);
        delAllButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert = new AlertDialog.Builder(getActivity());
                alert.setTitle(R.string.confirm);
                alert.setMessage(getString(R.string.delete_records, currentSlotName));
                alert.setPositiveButton(android.R.string.ok,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int whichButton) {
                                delRecords();
                                updateWebView();
                            }
                        });
                alert.setNegativeButton(android.R.string.cancel,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int whichButton) {
                            }
                        });
                alert.show();
            }
        });

        Button delRecordButton = (Button) rootView.findViewById(R.id.delRecordButton);
        delRecordButton
                .setBackgroundResource(R.drawable.transitionbottombuttons);

        TransitionDrawable transc = (TransitionDrawable) delRecordButton.getBackground();
        transc.startTransition(TRANSITION_DURATION);
        delRecordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRecordCount();
                if (records > 0) {
                    alert = new AlertDialog.Builder(getActivity());
                    alert.setTitle(getString(R.string.confirm));
                    String message = getString(R.string.delete_record, records, currentSlotName);
                    alert.setMessage(message);
                    alert.setPositiveButton(getString(android.R.string.ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int whichButton) {
                                    deleteLastRecord();
                                    updateWebView();
                                }
                            });
                    alert.setNegativeButton(getString(android.R.string.cancel),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int whichButton) {
                                }
                            });
                    alert.show();
                } else {
                    // give toast message to enter a number
                    String text = getString(R.string.no_records);
                    showToast(text);
                }
            }
        });

        Button addRecordButton = (Button) rootView.findViewById(R.id.addRecordButton);
        addRecordButton
                .setBackgroundResource(R.drawable.transitionbottombuttons);
        transa = (TransitionDrawable) addRecordButton.getBackground();
        transa.startTransition(TRANSITION_DURATION);
        addRecordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removePhoneKeypad();
                getTimeFromTextFields();
                straightenTime();
                storeAddTime();
                updateWebView();
                transa.startTransition(TRANSITION_DURATION);
            }
        });

        Button subRecordButton = (Button) rootView.findViewById(R.id.subRecordButton);
        subRecordButton
                .setBackgroundResource(R.drawable.transitionbottombuttons);
        transb = (TransitionDrawable) subRecordButton.getBackground();
        transb.startTransition(TRANSITION_DURATION);
        subRecordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removePhoneKeypad();
                getTimeFromTextFields();
                straightenTime();
                storeSubtractTime();
                updateWebView();
                transb.startTransition(TRANSITION_DURATION);
            }
        });

        slot1Button = (Button) rootView.findViewById(R.id.slotOneButton);
        slot1Button.setBackgroundResource(R.drawable.transitiontopbuttons);
        trans1 = (TransitionDrawable) slot1Button.getBackground();
        trans1.startTransition(TRANSITION_DURATION);
        slot1Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                removePhoneKeypad();
                previousSlot = currentSlot;
                currentSlot = 1;
                currentSlotName = slot1Name;
                updateWebView();

                if (currentSlot != previousSlot) {
                    switchPreviousButtonTransitionDrawable();
                    trans1.startTransition(TRANSITION_DURATION);
                }
            }
        });

        slot1Button.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View arg0) {
                alert = new AlertDialog.Builder(getActivity());
                renameInput = new EditText(getActivity());
                alert.setTitle(R.string.rename_string);
                alert.setMessage(R.string.type_new_and_ok);
                alert.setView(renameInput);
                alert.setPositiveButton(android.R.string.ok,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int whichButton) {
                                previousSlot = currentSlot;
                                currentSlot = 1;

                                currentSlotName = renameInput.getText()
                                        .toString();

                                currentSlotName = cleanInputString(currentSlotName);

                                slot1Name = currentSlotName;
                                slot1Button.setText(currentSlotName);
                                reNameSlot();
                                if (currentSlot != previousSlot) {
                                    switchPreviousButtonTransitionDrawable();
                                    trans1.startTransition(TRANSITION_DURATION);
                                }
                                updateWebView();
                            }

                        });
                alert.setNegativeButton(android.R.string.cancel,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int whichButton) {
                            }
                        });
                alert.show();
                return true;
            }
        });

        slot2Button = (Button) rootView.findViewById(R.id.slotTwoButton);
        slot2Button.setBackgroundResource(R.drawable.transitiontopbuttons);
        trans2 = (TransitionDrawable) slot2Button.getBackground();
        slot2Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                removePhoneKeypad();
                previousSlot = currentSlot;
                currentSlot = 2;
                currentSlotName = slot2Name;

                if (currentSlot != previousSlot) {
                    switchPreviousButtonTransitionDrawable();
                    trans2.startTransition(TRANSITION_DURATION);
                }
                updateWebView();
            }
        });

        slot2Button.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View arg0) {
                alert = new AlertDialog.Builder(getActivity());
                renameInput = new EditText(getActivity());
                alert.setTitle(R.string.rename_string);
                alert.setMessage(R.string.type_new_and_ok);
                alert.setView(renameInput);
                alert.setPositiveButton(android.R.string.ok,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int whichButton) {
                                previousSlot = currentSlot;
                                currentSlot = 2;
                                currentSlotName = renameInput.getText()
                                        .toString();

                                currentSlotName = cleanInputString(currentSlotName);

                                slot2Name = currentSlotName;
                                slot2Button.setText(currentSlotName);
                                reNameSlot();
                                if (currentSlot != previousSlot) {
                                    switchPreviousButtonTransitionDrawable();
                                    trans2.startTransition(TRANSITION_DURATION);
                                }
                                updateWebView();
                            }
                        });
                alert.setNegativeButton(android.R.string.cancel,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int whichButton) {
                            }
                        });
                alert.show();
                return true;
            }
        });

        updateTimerTextView();

        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_main, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_move_time:
                createMoveTimeDialog();
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * when resuming we want to have the user think the application is in
     * the state they left it. We can also update the view of records for that slot.
     */
    @Override
    public void onResume() {
        super.onResume();

        sharedPreferences = getActivity().getSharedPreferences(
                MY_PREFERENCES, Context.MODE_PRIVATE);

        if (sharedPreferences.contains(EDITTEXT_DAYS)) {
            setDaysText(sharedPreferences.getString(EDITTEXT_DAYS, ""));
        }

        if (sharedPreferences.contains(EDITTEXT_HOURS)) {
            setHoursText(sharedPreferences.getString(EDITTEXT_HOURS, ""));
        }

        if (sharedPreferences.contains(EDITTEXT_MINUTES)) {
            setMinutesText(sharedPreferences.getString(EDITTEXT_MINUTES, ""));
        }

        if (sharedPreferences.contains(EDITTEXT_SECONDS)) {
            setSecondsText(sharedPreferences.getString(EDITTEXT_SECONDS, ""));
        }

        if (sharedPreferences.contains(CURRENT_TIME_SPAN)) {
            currentTimeSpan = sharedPreferences.getLong(CURRENT_TIME_SPAN, 0);
        }

        if (sharedPreferences.contains(CLOSING_TIME)) {
            Long closingTime = sharedPreferences.getLong(CLOSING_TIME, 0);
            if (closingTime > 0) {
                pauseDate = new Date(closingTime);
            }
        }

        // check for previous timer is running if fail set to not running
        if (sharedPreferences.contains(TIMER_RUNNING)) {
            isTimerRunning = sharedPreferences
                    .getBoolean(TIMER_RUNNING, false);
        }

        if (isTimerRunning) {
            resumeTimerThread();
        }

        scoreDB = getActivity().openOrCreateDatabase(
                DATABASE_NAME,
                SQLiteDatabase.CREATE_IF_NECESSARY, null);
        scoreDB.execSQL("CREATE TABLE IF NOT EXISTS " + TIME_TRACKER_TABLE
                + " (" + COLUMN_ID + " INTEGER PRIMARY KEY, " + COLUMN_SLOT
                + " INTEGER, " + COLUMN_DAYS + " INTEGER, " + COLUMN_HOURS
                + " INTEGER, " + COLUMN_MINUTES + " INTEGER, " + COLUMN_SECONDS
                + " INTEGER," + COLUMN_COMMENT + " VARCHAR," + COLUMN_NAME
                + " VARCHAR) ");

        updateButtonNames();
        updateWebView();
        updateTimerTextView();

        if (pauseDate != null) {
            timeToAdd = new Date().getTime() - pauseDate.getTime();
            long seconds = TimeUnit.SECONDS
                    .convert(timeToAdd, TimeUnit.MILLISECONDS);

            long minutes = seconds / 60;
            seconds = seconds % 60;

            long hours = minutes / 60;
            minutes = minutes % 60;

            long days = hours / 24;
            hours = hours % 60;

            timerSecond = (int) seconds;
            timerMinute = (int) minutes;
            timerHour = (int) hours;
            timerDay = (int) days;

            String timeString = getString(R.string.time_string, timerDay, timerHour, timerMinute, timerSecond);

            if (timerDay + timerHour + timerMinute + timerSecond > 0) {
                alert = new AlertDialog.Builder(getActivity());
                alert.setTitle(R.string.add_time);
                String message = getString(R.string.timer_format, timeString);
                alert.setMessage(message);
                alert.setPositiveButton(android.R.string.yes,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int whichButton) {
                                currentTimeSpan += timeToAdd;
                                timeToAdd = 0L;
                                updateTimerTextView();
                            }
                        });
                alert.setNegativeButton(android.R.string.no,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int whichButton) {
                            }
                        });
                alert.create();
                testAlert = alert.show();
            }
        }
    }

    /**
     * The input string shouldn't have any carriage returns or whitespace.
     *
     * @param currentSlotName the current selected slot
     * @return the stripped down version of the currentSlotName
     */
    private String cleanInputString(String currentSlotName) {

        // remove returns in string
        currentSlotName = currentSlotName.replaceAll("[\n\r]", "");

        // remove excess white space
        currentSlotName = currentSlotName.trim().replaceAll("\\s+", " ");
        return currentSlotName;
    }

    /**
     * When adding or subtracting a time instance we need to parse the values
     * and add a zero for any missing values. We use exception handling to do
     * this, the parse integer call should throw an error if empty and we try to
     * turn it into an Integer.
     */
    private void getTimeFromTextFields() {
        try {
            days = Integer.parseInt(dTextView.getText().toString());
        } catch (NumberFormatException nfe1) {
            days = 0;
        }

        try {
            hours = Integer.parseInt(hTextView.getText().toString());
        } catch (NumberFormatException nfe1) {
            hours = 0;
        }

        try {
            minutes = Integer.parseInt(mTextView.getText().toString());
        } catch (NumberFormatException nfe1) {
            minutes = 0;
        }

        try {
            seconds = Integer.parseInt(sTextView.getText().toString());
        } catch (NumberFormatException nfe1) {
            seconds = 0;
        }

    }

    /**
     * All subtraction actions are stored as negative values so the total time
     * will be accurate when summing all transactions. For Example adding 1 minute and adding -30
     * seconds will show a total time of 30 seconds in our UI.
     */
    private void storeSubtractTime() {
        switch (currentSlot) {
            case 1:
                currentSlotName = slot1Name;
                break;
            case 2:
                currentSlotName = slot2Name;
                break;
        }

        // if one value is above 0 we have a valid time increment to work with
        if (days != 0 || hours != 0 || minutes != 0 || seconds != 0) {

            // store as a negative value
            days *= -1;
            hours *= -1;
            minutes *= -1;
            seconds *= -1;

            ContentValues values = new ContentValues();
            values.put(COLUMN_SLOT, currentSlot);
            values.put(COLUMN_DAYS, days);
            values.put(COLUMN_HOURS, hours);
            values.put(COLUMN_MINUTES, minutes);
            values.put(COLUMN_SECONDS, seconds);
            values.put(COLUMN_NAME, currentSlotName);
            scoreDB.insert(TIME_TRACKER_TABLE, null, values);

            // set back to all positive numbers on screen so they appear
            // as they should
            days *= -1;
            hours *= -1;
            minutes *= -1;
            seconds *= -1;

            if (days != 0) {
                dTextView.setText(String.valueOf(days));
            } else {
                dTextView.setText("");
            }

            if (hours != 0) {
                hTextView.setText(String.valueOf(hours));
            } else {
                hTextView.setText("");
            }

            if (minutes != 0) {
                mTextView.setText(String.valueOf(minutes));
            } else {
                mTextView.setText("");
            }

            if (seconds != 0) {
                sTextView.setText(String.valueOf(seconds));
            } else {
                sTextView.setText("");
            }

        } else {

            // give toast message to enter a number
            String text = getString(R.string.enter_time);
            showToast(text);
        }
    }

    /**
     * All addition actions are stored as positive values so the total time will
     * be accurate.
     */
    private void storeAddTime() {
        switch (currentSlot) {
            case 1:
                currentSlotName = slot1Name;
                break;
            case 2:
                currentSlotName = slot2Name;
                break;
        }

        // there needs to be at lease one value to be a valid time increment
        if (days != 0 || hours != 0 || minutes != 0 || seconds != 0) {
            ContentValues values = new ContentValues();
            values.put(COLUMN_SLOT, currentSlot);
            values.put(COLUMN_DAYS, days);
            values.put(COLUMN_HOURS, hours);
            values.put(COLUMN_MINUTES, minutes);
            values.put(COLUMN_SECONDS, seconds);
            values.put(COLUMN_NAME, currentSlotName);
            scoreDB.insert(TIME_TRACKER_TABLE, null, values);

            if (days != 0) {
                dTextView.setText(String.valueOf(days));
            } else {
                dTextView.setText("");
            }

            if (hours != 0) {
                hTextView.setText(String.valueOf(hours));
            } else {
                hTextView.setText("");
            }

            if (minutes != 0) {
                mTextView.setText(String.valueOf(minutes));
            } else {
                mTextView.setText("");
            }

            if (seconds != 0) {
                sTextView.setText(String.valueOf(seconds));
            } else {
                sTextView.setText("");
            }
        } else {
            // give toast message to enter a number
            String text = getString(R.string.please_enter);
            showToast(text);
        }
    }

    private void setRecordCount() {
        Cursor cursor = scoreDB.query(TIME_TRACKER_TABLE, new String[]{"*"},
                COLUMN_SLOT + "=" + currentSlot, null, null, null, null, null);
        records = cursor.getCount();
        cursor.close();
    }

    private void showToast(String message) {
        if (myToast != null) {
            myToast.cancel();
        }

        myToast = Toast
                .makeText(getActivity(), message, Toast.LENGTH_SHORT);
        myToast.setText(message);
        myToast.show();
    }

    @Override
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putString(getSeconds(), getString(R.string.seconds));
        bundle.putString(getMinutes(), getString(R.string.minutes));
        bundle.putString(getHours(), getString(R.string.hours));
        bundle.putString(getDays(), getString(R.string.days));
    }

    /**
     * On switching save slots we have an in transition for the button pressed
     * but we need to know which button was last in so we can perform a
     * transition out action.
     */
    private void switchPreviousButtonTransitionDrawable() {
        switch (previousSlot) {
            case 1:
                trans1.reverseTransition(TRANSITION_DURATION);
                break;
            case 2:
                trans2.reverseTransition(TRANSITION_DURATION);
                break;
            default:
                break;
        }

        // only one transition check needed at a time
        previousSlot = currentSlot;
    }

    /**
     * Time can roll over in seconds, minutes and days. So we may need to
     * straighten it out, for example 180 seconds turns to 3 minutes or -120 = -2 minutes.
     * Negative values reflect a subtract time increment.
     */
    public void straightenTime() {
        TimeAmounts timeAmounts = new TimeAmounts(days, hours, minutes, seconds);
        days = timeAmounts.days;
        hours = timeAmounts.hours;
        minutes = timeAmounts.minutes;
        seconds = timeAmounts.seconds;
    }

    /**
     * Long press on a save slot allows for it to be renamed.
     */
    private void reNameSlot() {
        ContentValues args = new ContentValues();
        if (currentSlotName.length() >= 10) {
            currentSlotName = currentSlotName.substring(0, 10);
        }
        args.put(COLUMN_NAME, currentSlotName);
        scoreDB.update(TIME_TRACKER_TABLE, args, COLUMN_SLOT + "="
                + currentSlot, null);
    }

    /**
     * Logic for button press to reset save slot name.
     */
    private void resetCurrentSlotName() {
        switch (currentSlot) {
            case 1:
                currentSlotName = slot1Name = DEFAULT_SLOT_ONE;
                slot1Button.setText(slot1Name);
                break;
            case 2:
                currentSlotName = slot2Name = DEFAULT_SLOT_TWO;
                slot2Button.setText(slot2Name);
                break;
        }
    }

    private void deleteLastRecord() {

        // gets the last record and removes it
        Cursor cursor = scoreDB.query(TIME_TRACKER_TABLE, new String[]{"*"},
                COLUMN_SLOT + "=" + currentSlot, null, null, null, null, null);

        cursor.moveToLast();
        if (cursor.getCount() > 0) {
            scoreDB.delete(TIME_TRACKER_TABLE,
                    COLUMN_ID + "=" + cursor.getString(0), null);
        }
        cursor.close();
    }

    private void delRecords() {
        days = hours = minutes = seconds = 0;
        dTextView.setText("");
        hTextView.setText("");
        mTextView.setText("");
        sTextView.setText("");
        resetCurrentSlotName();
        scoreDB.delete(TIME_TRACKER_TABLE, COLUMN_SLOT + "=" + currentSlot,
                null);
    }

    /**
     * Display the records associated with the current slot selected.
     */
    private void updateWebView() {
        webview.loadUrl("about:blank");

        Cursor cursor = scoreDB.query(TIME_TRACKER_TABLE, new String[]{"*"},
                COLUMN_SLOT + "=" + currentSlot, null, null, null, null, null);

        StringBuilder builder = new StringBuilder();
        builder.append("<table width='100%25'>");
        builder.append("<tr><td><h4>");
        builder.append("<FONT COLOR=00CC00># Name</FONT>");
        builder.append("</h4></td>");
        builder.append("<td><h4>");
        builder.append("<FONT COLOR=00CC00>Days</FONT>");
        builder.append("</h4></td>");
        builder.append("<td><h4>");
        builder.append("<FONT COLOR=00CC00>Hrs.</FONT>");
        builder.append("</h4></td>");
        builder.append("<td><h4>");
        builder.append("<FONT COLOR=00CC00>Min.</FONT>");
        builder.append("</h4></td>");
        builder.append("<td><h4>");
        builder.append("<FONT COLOR=00CC00>Sec.</FONT>");
        builder.append("</h4></td></tr>");

        cursor.moveToLast();

        days = hours = minutes = seconds = 0;

        int acc = 0;
        for (int i = cursor.getCount() - 1; i >= 0; i--) {
            builder.append("<tr>");
            builder.append("<td><h3><FONT COLOR=FFFFFF>").append(cursor.getCount() - acc).append(" ").append(cursor.getString(7)).append("</FONT></h3></td>");
            builder.append("<td><h3><FONT COLOR=FFFFFF>");
            days += Integer.parseInt(cursor.getString(2));
            builder.append(cursor.getString(2));
            builder.append("</FONT></h3></td>");
            builder.append("<td><h3><FONT COLOR=FFFFFF>");
            hours += Integer.parseInt(cursor.getString(3));
            builder.append(cursor.getString(3));
            builder.append("</FONT></h3></td>");
            builder.append("<td><h3><FONT COLOR=FFFFFF>");
            minutes += Integer.parseInt(cursor.getString(4));
            builder.append(cursor.getString(4));
            builder.append("</FONT></h3></td>");
            builder.append("<td><h3><FONT COLOR=FFFFFF>");
            seconds += Integer.parseInt(cursor.getString(5));
            builder.append(cursor.getString(5));
            builder.append("</FONT></h3></td>");
            builder.append("</tr>");
            acc++;
            cursor.moveToPrevious();
        }
        builder.append("</table></body></html>");

        cursor.close();
        straightenTime();

        builder.insert(
                0,
                "<html><body><h3><FONT COLOR=00CC00><center>Scrollable Records"
                        + "</center></FONT></h3><h4><FONT COLOR=FFFFFF><center>"
                        + currentSlotName
                        + "</FONT COLOR=FFFFFF><FONT COLOR=00CC00>: " + days
                        + " days " + hours + " hrs. " + minutes + " min. "
                        + seconds + " sec.</center></FONT></h4>");

        webview.loadData(builder.toString(), "text/html", "UTF-8");
        days = hours = minutes = seconds = 0;
    }

    /**
     * Restore user modified save slot names.
     */
    private void updateButtonNames() {

        // record an instance of each named slot
        Cursor cursor = scoreDB.query(TIME_TRACKER_TABLE, new String[]{"*"},
                null, null, null, null, null, null);
        cursor.moveToLast();
        ArrayList<Integer> numbers = new ArrayList<>();

        for (int i = cursor.getCount() - 1; i >= 0; i--) {
            int slot = Integer.parseInt(cursor.getString(1));
            if (!numbers.contains(slot)) {
                numbers.add(slot);
                String name = cursor.getString(7);
                setSlotName(slot, name);
            }
            cursor.moveToPrevious();
        }

        cursor.close();

        switch (currentSlot) {
            case 1:
                currentSlotName = slot1Name;
                break;
            case 2:
                currentSlotName = slot2Name;
                break;
        }
    }

    private void setSlotName(int slotInt, String slotString) {
        switch (slotInt) {
            case 1:
                slot1Name = slotString;
                slot1Button.setText(slot1Name);
                break;
            case 2:
                slot2Name = slotString;
                slot2Button.setText(slot2Name);
                break;
        }
    }

    private void removePhoneKeypad() {
        InputMethodManager inputManager = (InputMethodManager) getActivity()
                .getSystemService(Context.INPUT_METHOD_SERVICE);

        IBinder binder = rootView.getWindowToken();
        inputManager.hideSoftInputFromWindow(binder,
                InputMethodManager.HIDE_NOT_ALWAYS);
    }

    private void resumeTimerThread() {
        if (timerThread == null
                || timerThread.getState() == Thread.State.TERMINATED) {
            startDate = new Date();
            timerThread = new TimerThread();
            timerThread.start();
            isTimerRunning = true;
        }
    }

    private void startNewTimerThread() {
        if (timerThread == null
                || timerThread.getState() == Thread.State.TERMINATED) {

            currentTimeSpan = 0;

            startDate = new Date();
            timerThread = new TimerThread();
            timerThread.start();
            isTimerRunning = true;
        }
    }

    private void timerToggle() {
        if (!isTimerRunning) {

            //startDate new time span and new timer thread to update it
            if (startDate == null && updateDate == null) {

                //check starting new or resuming timer
                if (currentTimeSpan == 0) {
                    startNewTimerThread();
                    showToast(getString(R.string.timer_started));
                } else {
                    resumeTimerThread();
                    showToast(getString(R.string.timer_resumed));
                }
            } else {

                // give the user the option to resume or restart the timer
                alert = new AlertDialog.Builder(getActivity());
                alert.setMessage(R.string.resume_restart);
                alert.setNegativeButton(R.string.resume_timer,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int whichButton) {
                                resumeTimerThread();
                                showToast(getString(R.string.timer_resumed));
                            }
                        });
                alert.setPositiveButton(R.string.restart_timer,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int whichButton) {
                                // call method to parse values and add to edit
                                // text fields
                                startNewTimerThread();
                                showToast(getString(R.string.timer_restarted));
                            }
                        });
                alert.setCancelable(true);
                alert.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {

                    }
                });
                alert.show();
            }
        } else {

            //pause timer if its running
            pauseTimerThread();

            //update current time span
            updateCurrentTimeSpan();

            //update UI
            updateTimerTextView();
            showToast(getString(R.string.timer_paused));
        }
    }

    private void updateCurrentTimeSpan() {

        // most recent timer start time
        long startMillis = startDate.getTime();

        // most recent timer end time
        long endMillis = updateDate.getTime();

        // add this time span to the running total
        long latestTimeSpan = endMillis - startMillis;
        currentTimeSpan = currentTimeSpan + latestTimeSpan;
    }

    /**
     * Update the time span from the running thread, called every second if the thread is running.
     */
    public void updateRunningTimeSpan() {

        //store the started time
        long startMillis = startDate.getTime();

        //reset the start date
        startDate = new Date();

        // get current time
        long currentMillis = startDate.getTime();

        // find time span and add to running total
        long latestCurrentTimeSpan = currentMillis - startMillis;
        currentTimeSpan += latestCurrentTimeSpan;

        //update the UI
        updateTimerTextView();
    }

    public void updateTimerTextView() {

        // ensure the fragment is added so there is an activity to pull resources from
        if (isAdded()) {
            long seconds = TimeUnit.SECONDS
                    .convert(currentTimeSpan, TimeUnit.MILLISECONDS);

            long minutes = seconds / 60;
            seconds = seconds % 60;

            long hours = minutes / 60;
            minutes = minutes % 60;

            long days = hours / 24;
            hours = hours % 60;

            timerSecond = (int) seconds;
            timerMinute = (int) minutes;
            timerHour = (int) hours;
            timerDay = (int) days;
            timerTextString = getString(R.string.time_string, timerDay, timerHour, timerMinute, timerSecond);
            timerTextView.setText(timerTextString);
        }
    }

    private void sendRunningTimerToInputFields(int timerDay, int timerHour, int timerMinute, int timerSecond) {
        int total = timerDay + timerHour + timerMinute + timerSecond;
        if (total > 0) {

            // get difference of pause from startDate time and print values in edit texts
            dTextView.setText(String.valueOf(timerDay));
            hTextView.setText(String.valueOf(timerHour));
            mTextView.setText(String.valueOf(timerMinute));
            sTextView.setText(String.valueOf(timerSecond));

            // fill in hint for values above timer amount
            if (timerDay == 0) {
                dTextView.setText("");
                if (timerHour == 0) {
                    hTextView.setText("");
                    if (timerMinute == 0) {
                        mTextView.setText("");
                    }
                }
            }
        } else {
            showToast(getString(R.string.press_timer));
        }
    }

    private void pauseTimerThread() {
        if (timerThread != null && timerThread.isAlive()) {
            updateDate = new Date();
            timerThread.interrupt();
            timerThread = null;
            isTimerRunning = false;
        }
    }

    public void setSecondsText(String seconds) {
        if (sTextView != null) {
            sTextView.setText(seconds);
        }
    }

    public void setMinutesText(String minutes) {
        if (mTextView != null) {
            mTextView.setText(minutes);
        }
    }

    public void setHoursText(String hours) {
        if (hTextView != null) {
            hTextView.setText(hours);
        }
    }

    public void setDaysText(String days) {
        if (dTextView != null) {
            dTextView.setText(days);
        }
    }

    public String getSeconds() {
        return sTextView.getText().toString();
    }

    public String getMinutes() {
        return mTextView.getText().toString();
    }

    public String getHours() {
        return hTextView.getText().toString();
    }

    public String getDays() {
        return dTextView.getText().toString();
    }

    public void createMoveTimeDialog() {
        updateTimerTextView();

        String message = getString(R.string.send_format, timerTextString);
        alert = new AlertDialog.Builder(getActivity());
        alert.setTitle(getString(R.string.confirm));
        alert.setMessage(message);

        //store times into final into to keep references while timer is running.
        final int day = timerDay;
        final int hour = timerHour;
        final int minute = timerMinute;
        final int second = timerSecond;
        alert.setPositiveButton(getString(android.R.string.ok),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,
                                        int whichButton) {
                        sendRunningTimerToInputFields(day, hour, minute, second);
                        timerTrans.startTransition(TRANSITION_DURATION);
                    }
                });
        alert.setNegativeButton(getString(android.R.string.cancel),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,
                                        int whichButton) {
                    }
                });
        alert.show();
    }

    @Override
    public void onPause() {
        super.onPause();
        pauseDate = new Date();
    }

    /**
     * On stop of the application, the only item we need to track in our time
     * tracker is the timer thread that is running and if its supposed to be
     * running on resume.
     */
    @Override
    public void onStop() {
        super.onStop();
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(TIMER_RUNNING, isTimerRunning);
        if (isTimerRunning) {
            pauseTimerThread();
        }
        editor.putLong(CLOSING_TIME, new Date().getTime());
        editor.putLong(CURRENT_TIME_SPAN, currentTimeSpan);
        editor.putString(EDITTEXT_DAYS, getDays());
        editor.putString(EDITTEXT_HOURS, getHours());
        editor.putString(EDITTEXT_MINUTES, getMinutes());
        editor.putString(EDITTEXT_SECONDS, getSeconds());
        editor.apply();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (scoreDB.isOpen()) {
            scoreDB.close();
        }
        if (isTimerRunning) {
            pauseTimerThread();
        }
    }


    /**
     * A class to hold the thread and logic for the timer object for a given task.
     *
     * @author Rick
     */
    private class TimerThread extends Thread {

        /**
         * While the thread is running we can see that sleep is called every second
         * and when it wakes up we update the view and sleep for another second.
         * The view must be updated on the UI thread.
         */
        @Override
        public void run() {
            while (isTimerRunning) {
                Activity act = getActivity();
                act.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        updateRunningTimeSpan();
                    }
                });

                if (isTimerRunning) {
                    try {
                        Thread.sleep(ONE_SECOND);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
}