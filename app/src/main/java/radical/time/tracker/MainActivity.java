package radical.time.tracker;

import android.app.Dialog;
import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

/**
 * A class to start the activity code.
 *
 * @author Rick
 */
public class MainActivity extends AppCompatActivity {

    /**
     * On create of the Activity we need to inflate the view when we set it as
     * content. We also check that the user's phone is using the current google
     * play store version so our admob can work.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);
        Integer resultCode = GooglePlayServicesUtil
                .isGooglePlayServicesAvailable(this);
        switch (resultCode) {
            case ConnectionResult.SUCCESS:
                // Do what you want
                break;
            default:
                Dialog dialog = GooglePlayServicesUtil.getErrorDialog(resultCode,
                        this, 0);
                if (dialog != null) {
                    // This dialog will help the user update to the latest
                    // GooglePlayServices
                    dialog.show();
                }
                break;
        }
    }

    /**
     * We have to implement the ad view in a fragment, this will call the
     * request to build and show. Per the docs we resume, pause and destroy the
     * ad view as well with the Activity.
     *
     * @author Rick
     */
    public static class AdFragment extends Fragment {

        public AdView mAdView;

        @Override
        public void onActivityCreated(Bundle bundle) {
            super.onActivityCreated(bundle);
            View view = getView();
            if (view != null) {
                mAdView = (AdView) view.findViewById(R.id.adView);

                //block your test devices, check android monitor log for your device id
                AdRequest adRequest = new AdRequest.Builder().addTestDevice("BF1D9B0CDDFBA0C161EBF8FB3A01A773").build();
                mAdView.loadAd(adRequest);
            }
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            return inflater.inflate(R.layout.fragment_ad, container, false);
        }

        @Override
        public void onResume() {
            super.onResume();

            // Resume the AdView.
            mAdView.resume();
        }

        @Override
        public void onPause() {

            // Pause the AdView.
            mAdView.pause();

            super.onPause();
        }

        @Override
        public void onDestroy() {

            // Destroy the AdView.
            mAdView.destroy();

            super.onDestroy();
        }
    }
}