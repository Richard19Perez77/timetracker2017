package radical.time.tracker;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class TimeAmountsPositiveUnitTest {

    @Test
    public void straightenTime0() throws Exception {
        TimeAmounts timeAmounts = new TimeAmounts(0, 0, 0, 0);
        assertEquals(timeAmounts.days, 0);
        assertEquals(timeAmounts.hours, 0);
        assertEquals(timeAmounts.minutes, 0);
        assertEquals(timeAmounts.seconds, 0);
    }

    @Test
    public void straightenTime1MinuteFromSeconds() throws Exception {
        TimeAmounts timeAmounts = new TimeAmounts(0, 0, 0, 60);
        assertEquals(timeAmounts.days, 0);
        assertEquals(timeAmounts.hours, 0);
        assertEquals(timeAmounts.minutes, 1);
        assertEquals(timeAmounts.seconds, 0);
    }

    @Test
    public void straightenTime1HourFromSeconds() throws Exception {
        TimeAmounts timeAmounts = new TimeAmounts(0, 0, 0, 3600);
        assertEquals(timeAmounts.days, 0);
        assertEquals(timeAmounts.hours, 1);
        assertEquals(timeAmounts.minutes, 0);
        assertEquals(timeAmounts.seconds, 0);
    }

    @Test
    public void straightenTime1DayFromSeconds() throws Exception {
        TimeAmounts timeAmounts = new TimeAmounts(0, 0, 0, 86400);
        assertEquals(timeAmounts.days, 1);
        assertEquals(timeAmounts.hours, 0);
        assertEquals(timeAmounts.minutes, 0);
        assertEquals(timeAmounts.seconds, 0);
    }

    @Test
    public void straightenTime1Day1HourFromSeconds() throws Exception {
        TimeAmounts timeAmounts = new TimeAmounts(0, 0, 0, 90000);
        assertEquals(timeAmounts.days, 1);
        assertEquals(timeAmounts.hours, 1);
        assertEquals(timeAmounts.minutes, 0);
        assertEquals(timeAmounts.seconds, 0);
    }

    @Test
    public void straightenTime1Day1Hour1MinuteFromSeconds() throws Exception {
        TimeAmounts timeAmounts = new TimeAmounts(0, 0, 0, 90060);
        assertEquals(timeAmounts.days, 1);
        assertEquals(timeAmounts.hours, 1);
        assertEquals(timeAmounts.minutes, 1);
        assertEquals(timeAmounts.seconds, 0);
    }

    @Test
    public void straightenTime1HourFromMinutes() throws Exception {
        TimeAmounts timeAmounts = new TimeAmounts(0, 0, 60, 0);
        assertEquals(timeAmounts.days, 0);
        assertEquals(timeAmounts.hours, 1);
        assertEquals(timeAmounts.minutes, 0);
        assertEquals(timeAmounts.seconds, 0);
    }

    @Test
    public void straightenTime1DayFromMinutes() throws Exception {
        TimeAmounts timeAmounts = new TimeAmounts(0, 0, 1440, 0);
        assertEquals(timeAmounts.days, 1);
        assertEquals(timeAmounts.hours, 0);
        assertEquals(timeAmounts.minutes, 0);
        assertEquals(timeAmounts.seconds, 0);
    }

    @Test
    public void straightenTime1Day1HourFromMinutes() throws Exception {
        TimeAmounts timeAmounts = new TimeAmounts(0, 0, 1500, 0);
        assertEquals(timeAmounts.days, 1);
        assertEquals(timeAmounts.hours, 1);
        assertEquals(timeAmounts.minutes, 0);
        assertEquals(timeAmounts.seconds, 0);
    }

    @Test
    public void straightenTimeSeconds() throws Exception {
        int seconds = 59;
        while (seconds >= 0) {
            System.out.println("seconds: " + seconds);
            TimeAmounts timeAmounts = new TimeAmounts(0, 0, 0, seconds);
            assertEquals(timeAmounts.days, 0);
            assertEquals(timeAmounts.hours, 0);
            assertEquals(timeAmounts.minutes, 0);
            assertEquals(timeAmounts.seconds, seconds);
            seconds--;
        }
    }

    @Test
    public void straightenTimeSecondsLeft() throws Exception {
        int seconds = 120;
        while (seconds >= 0) {
            System.out.println("seconds: " + seconds);
            TimeAmounts timeAmounts = new TimeAmounts(0, 0, 0, seconds);
            assertEquals(timeAmounts.days, 0);
            assertEquals(timeAmounts.hours, 0);
            assertEquals(timeAmounts.minutes, seconds / 60);
            assertEquals(timeAmounts.seconds, seconds % 60);
            seconds--;
        }
    }

    @Test
    public void straightenTimeMinutesLeft() throws Exception {
        int minutes = 120;
        while (minutes >= 0) {
            TimeAmounts timeAmounts = new TimeAmounts(0, 0, minutes, 0);
            assertEquals(timeAmounts.days, 0);
            assertEquals(timeAmounts.hours, minutes / 60);
            assertEquals(timeAmounts.minutes, minutes % 60);
            assertEquals(timeAmounts.seconds, 0);
            minutes--;
        }
    }

    @Test
    public void straightenTimeHoursLeft() throws Exception {
        int hours = 48;
        while (hours >= 0) {
            TimeAmounts timeAmounts = new TimeAmounts(0, hours, 0, 0);
            assertEquals(timeAmounts.days, hours / 24);
            assertEquals(timeAmounts.hours, hours % 24);
            assertEquals(timeAmounts.minutes, 0);
            assertEquals(timeAmounts.seconds, 0);
            hours--;
        }
    }
}